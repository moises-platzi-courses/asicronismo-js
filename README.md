# Asicronismo con JavaScript

Apropia los conceptos fundamentales de asincronismo con JavaScript, aplica sus diferentes estructuras y desarrolla soluciones asíncronas.

+ Crea una landing page y realiza el despliegue en GitHub Pages.
+ Aplica funciones asíncronas a tus proyectos para interactuar con una API.
+ Entiende los conceptos de Event Loop, Callbacks y promesas.
+ Aprende los conceptos de asincronismo con JavaScript.

## Contenido del Curso

### Introducción
+ Qué es el asincronismo
+ Event Loop

### Callbacks
+ Qué son los Callbacks
+ XMLHTTPRequest
+ Fetch data
+ Callback hell

### Promesas
+ Qué son las promesas
+ Fetch
+ Fetch POST

### Async Await
+ Funciones asíncronas
+ Try and catch
+ ¿Cómo enfrentar los errores?

### Generadores
+ Generators
