const XMLHttpRequest = require('xmlhttprequest'); // llamado al XmlHttpRequest
const API = 'https://api.escuelajs.co/api/v1'; // API en mayúscula porque es una referencia que no va cambiar

function fetchData(urlApi, callback) {
  let xhttp = new XMLHttpRequest();

  xhttp.open('GET', urlApi, true);
  xhttp.onreadystatechange = function (event) {
    // escucha diferentes estados de la solicitud y conocer cuando está disponible la información
    if (xhttp.readyState === 4) {
      // si el estado ha sido completada la llamada
      if (xhttp.status === 200) {
        // el servidor reponde de forma correcta
        callback(null, JSON.parse(xhttp.responseText)); // dentro de xhttp.responseTex recibimos lo que entrega el servidor en texto y se hace la transformación en JSON
      }
    } else {
      const error = new Error('Error' + urlApi);
      return callback(error, null); // es null porque no se está regresando ningún dato
    }
  }
  xhttp.send();
}